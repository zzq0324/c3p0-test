#c3p0-test

##项目说明
&nbsp;&nbsp;&nbsp;&nbsp;根据c3p0的配置参数进行代码测试，验证各个参数的具体含义。除了driverClass、jdbcUrl、user和password参数为必填项，其他均为选填项。详细可参考[c3p0参数配置](http://www.mchange.com/projects/c3p0/#configuration_properties)
    
##配置参数
&nbsp;&nbsp;&nbsp;&nbsp;c3p0配置参数的默认值，统一配置在com.mchange.v2.c3p0.impl.C3P0Defaults类中。

* `acquireIncrement`：连接池在无空闲连接可用时一次性获取的新数据库连接数，默认为3;
* `initialPoolSize`：连接池初始时创建的连接数，默认为3。比较好的配置是初始连接数介于minPoolSize和maxPoolSize之间，如果超过maxPoolSize，则设置为maxPoolSize，如果小宇minPoolSize，则设置为minPoolSize。备注：该说法对于0.9.5.2的版本有效，对0.9.5-pre8版本无效，怀疑是0.9.5-pre8版本的bug;
* `minPoolSize`：连接池中保持的最小连接数，默认为3;
* `maxPoolSize`：连接池中保持的最大连接数，默认为15;
* `maxStatements`：连接池中缓存的最大statement个数，默认为0。maxStatements是标准的JDBC参数，代表全局的statement缓存个数，超过maxStatements之后会采用LRU的原则剔除最近使用频率比较低的statement缓存，但是由于statement的缓存都是属于各个connection的，因此实际运行过程中维护该缓存有一定的开销，因此设置该值需要谨慎。实际情况中maxStatements会和maxStatementsPerConnection结合分析，如果两个值都小于等于0，则关闭缓存；只要有其中一个大于0，则开启缓存。缓存的个数如果两个参数都大于0，则所有connection的statement缓存个数受maxStatements限制，单个connection的statement缓存受maxStatementsPerConnection限制。详见[jdbc statement cache](http://blog.bertvanlangen.com/software-development/jdbc-statement-cache/);
* `maxStatementsPerConnection`：每个connection中缓存的statement的最大个数，默认为0，代表不缓存;
* `idleConnectionTestPeriod`：每隔多久检测下连接池中的空闲连接是否仍然有效，默认为0，代表不检测。Mysql默认8小时后会断开已打开的连接，因此加上该参数可以对连接进行检测，在断开之后自动连接;
* `maxIdleTime`：每隔多少秒检测下连接池中的空闲连接，如果超过maxIdleTime则关闭连接，连接池中最少保留minPoolSize个连接。默认为0，代表永不丢弃空闲连接;