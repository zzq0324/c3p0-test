package org.simplestudio.c3p0.datasource;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.simplestudio.c3p0.dao.UserDAO;
import org.simplestudio.c3p0.util.PropertyUtil;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.mchange.v2.c3p0.DataSources;
import com.mchange.v2.c3p0.impl.C3P0Defaults;

/**   
* @Title: C3p0ConfigTest.java 
* @Description: c3p0的配置项测试 
* @author zhengzhq E-Mail:zzq0324@qq.com
* @date 2016年10月8日 上午9:52:53 
*/
public class C3p0ConfigTest {

    @Test
    public void testPoolSize() throws Exception {
        //获取默认的配置
        Properties defaultProp = PropertyUtil.getConfig("c3p0-default");
        ComboPooledDataSource defaultDataSource = DataSourceFactory.initDataSource(defaultProp);
        Assert.assertNotNull(defaultDataSource);
        Assert.assertEquals(C3P0Defaults.initialPoolSize(), defaultDataSource.getInitialPoolSize());
        Assert.assertEquals(C3P0Defaults.minPoolSize(), defaultDataSource.getMinPoolSize());
        Assert.assertEquals(C3P0Defaults.maxPoolSize(), defaultDataSource.getMaxPoolSize());

        //获取自定义的配置
        Properties customProp = PropertyUtil.getConfig("c3p0-custom");
        ComboPooledDataSource customDataSource = DataSourceFactory.initDataSource(customProp);
        Assert.assertNotNull(customDataSource);
        Assert.assertEquals(Integer.parseInt(customProp.get("c3p0.initialPoolSize").toString()),
                customDataSource.getInitialPoolSize());
        Assert.assertEquals(Integer.parseInt(customProp.get("c3p0.minPoolSize").toString()),
                customDataSource.getMinPoolSize());
        Assert.assertEquals(Integer.parseInt(customProp.get("c3p0.maxPoolSize").toString()),
                customDataSource.getMaxPoolSize());

        //异常的情况，initialPoolSize比minSize还小
        Properties specialProp = PropertyUtil.getConfig("c3p0-special");
        ComboPooledDataSource specialDataSource = DataSourceFactory.initDataSource(specialProp);
        Assert.assertEquals(Integer.parseInt(specialProp.get("c3p0.initialPoolSize").toString()),
                specialDataSource.getInitialPoolSize());
        Assert.assertEquals(Integer.parseInt(specialProp.get("c3p0.minPoolSize").toString()),
                specialDataSource.getMinPoolSize());
        Assert.assertEquals(Integer.parseInt(specialProp.get("c3p0.maxPoolSize").toString()),
                specialDataSource.getMaxPoolSize());

        /******************* 测试连接 *********************/
        try {
            //获取下连接，确保numberConnections有初始
            defaultDataSource.getConnection();
            //休眠，确保连接初始完毕
            Thread.sleep(500);
            Assert.assertEquals(C3P0Defaults.initialPoolSize(),
                    defaultDataSource.getNumConnections());
        } finally {
            DataSources.destroy(defaultDataSource);
        }

        try {
            customDataSource.getConnection();
            //休眠，确保连接初始完毕
            Thread.sleep(500);
            Assert.assertEquals(Integer.parseInt(customProp.get("c3p0.initialPoolSize").toString()),
                    customDataSource.getNumConnections());
        } finally {
            DataSources.destroy(customDataSource);
        }

        try {
            specialDataSource.getConnection();
            //休眠，确保连接初始完毕
            Thread.sleep(500);
            //此处的测试如果在0.9.5-pre8版本下，initialPoolSize可以超过maxPoolSize，在0.9.5.2版本下是最大的连接池
            Assert.assertEquals(Integer.parseInt(specialProp.get("c3p0.maxPoolSize").toString()),
                    specialDataSource.getNumConnections());
        } finally {
            DataSources.destroy(specialDataSource);
        }
        /******************* 测试连接 *********************/
    }

    @Test
    public void testAcquireIncrement() throws Exception {
        //获取默认的配置
        Properties prop = PropertyUtil.getConfig("c3p0-default");
        ComboPooledDataSource dataSource = DataSourceFactory.initDataSource(prop);
        Assert.assertNotNull(dataSource);

        try {
            //acquireIncrement默认是3
            Assert.assertEquals(C3P0Defaults.acquireIncrement(), dataSource.getAcquireIncrement());

            //模拟获取连接，获取之后，空闲连接数应当-1
            dataSource.getConnection();
            Thread.sleep(500);
            Assert.assertEquals(C3P0Defaults.initialPoolSize(), dataSource.getNumConnections());
            for (int i = 1; i < 3; i++) {
                dataSource.getConnection();
                Assert.assertEquals(C3P0Defaults.initialPoolSize() - i - 1,
                        dataSource.getNumIdleConnections());
            }

            //继续取一个连接，此时连接池已无空闲连接，会一次获取acquireIncrement个连接
            dataSource.getConnection();
            Thread.sleep(500);
            Assert.assertEquals(C3P0Defaults.initialPoolSize() + C3P0Defaults.acquireIncrement(),
                    dataSource.getNumConnections());
        } finally {
            DataSources.destroy(dataSource);
        }

        //获取自定义的配置
        Properties customProp = PropertyUtil.getConfig("c3p0-custom");
        ComboPooledDataSource customDataSource = DataSourceFactory.initDataSource(customProp);
        try {
            //模拟获取连接，获取之后，空闲连接数应当-1
            customDataSource.getConnection();
            Thread.sleep(500);
            Assert.assertEquals(Integer.parseInt(customProp.get("c3p0.initialPoolSize").toString()),
                    customDataSource.getNumConnections());
            for (int i = 1; i < 15; i++) {
                customDataSource.getConnection();
                Assert.assertEquals(
                        Integer.parseInt(customProp.get("c3p0.initialPoolSize").toString()) - i - 1,
                        customDataSource.getNumIdleConnections());
            }

            //继续取一个连接，此时连接池已无空闲连接，会一次获取acquireIncrement个连接
            customDataSource.getConnection();
            Thread.sleep(500);
            Assert.assertEquals(
                    Integer.parseInt(customProp.get("c3p0.initialPoolSize").toString())
                            + Integer.parseInt(customProp.get("c3p0.acquireIncrement").toString()),
                    customDataSource.getNumConnections());
        } finally {
            DataSources.destroy(customDataSource);
        }
    }

    @Test
    @Ignore
    public void testStatements() throws Exception {
        String dropSql = "drop table if exists t_user ";
        String createSql = "create table t_user(id bigint)";
        Properties customProp = PropertyUtil.getConfig("c3p0-custom");
        ComboPooledDataSource customDataSource = DataSourceFactory.initDataSource(customProp);
        Connection conn = customDataSource.getConnection();
        conn.prepareStatement(dropSql);
        conn.prepareStatement(createSql);
        conn.close();

        UserDAO userDAO = new UserDAO();

        userDAO.query(customDataSource.getConnection(), 1);
    }

    @Test
    public void testMaxIdleTime() throws Exception {
        //获取自定义的配置
        Properties customProp = PropertyUtil.getConfig("c3p0-custom");
        ComboPooledDataSource customDataSource = DataSourceFactory.initDataSource(customProp);

        try {
            int initialPoolSize = Integer.parseInt(customProp.getProperty("c3p0.initialPoolSize"));
            int minPoolSize = Integer.parseInt(customProp.getProperty("c3p0.minPoolSize"));
            int acquireIncrement = Integer
                    .parseInt(customProp.getProperty("c3p0.acquireIncrement"));
            List<Connection> list = new ArrayList<Connection>();
            //将初始的连接用完
            for (int i = 0; i < initialPoolSize; i++) {
                list.add(customDataSource.getConnection());
            }

            //继续获取连接，获取完关闭，确保等会可以回收
            customDataSource.getConnection().close();
            Thread.sleep(500);
            Assert.assertEquals(initialPoolSize + acquireIncrement,
                    customDataSource.getNumConnections());

            //休眠maxIdleTime+1秒的时间，确保到了空闲连接回收
            int maxIdleTime = Integer.parseInt(customProp.getProperty("c3p0.maxIdleTime"));
            Assert.assertEquals(maxIdleTime, customDataSource.getMaxIdleTime());
            //休眠maxIdleTime秒，此时再去取连接
            Thread.sleep(maxIdleTime * 1000);
            Assert.assertEquals(initialPoolSize, customDataSource.getNumConnections());

            //关闭之前的连接
            for (int i = 0; i < list.size(); i++) {
                list.get(i).close();
            }

            //休眠maxIdleTime秒，此时再去取连接
            Thread.sleep(maxIdleTime * 1000 * 2);
            Assert.assertEquals(minPoolSize, customDataSource.getNumConnections());
        } finally {
            DataSources.destroy(customDataSource);
        }
    }

}
