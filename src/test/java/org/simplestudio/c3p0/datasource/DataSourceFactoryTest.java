package org.simplestudio.c3p0.datasource;

import java.sql.Connection;
import java.util.Properties;

import org.junit.Test;
import org.simplestudio.c3p0.util.PropertyUtil;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.mchange.v2.c3p0.DataSources;

import junit.framework.Assert;

/**   
* @Title: DataSourceFactoryTest.java 
* @Description:  DataSourceFactory的测试
* @author zhengzhq E-Mail:zzq0324@qq.com
* @date 2016年9月30日 上午11:54:37 
*/
public class DataSourceFactoryTest {

    @Test
    public void testInitDataSourceBean() throws Exception {
        Properties prop = PropertyUtil.getConfig("c3p0-default");
        ComboPooledDataSource dataSource = DataSourceFactory.initDataSource(prop);
        Assert.assertNotNull(dataSource);
        DataSources.destroy(dataSource);
    }

    @Test
    public void testGetConnection() throws Exception {
        Properties prop = PropertyUtil.getConfig("c3p0-custom");
        ComboPooledDataSource dataSource = DataSourceFactory.initDataSource(prop);
        Assert.assertNotNull(dataSource);
        Connection conn = dataSource.getConnection();
        Assert.assertNotNull(conn);

        conn.close();
        DataSources.destroy(dataSource);
    }
}
