package org.simplestudio.c3p0.util;

import java.util.Properties;

import org.junit.Test;

import junit.framework.Assert;

/**   
* @Title: PropertyUtilTest.java 
* @Description: PropertyUtil的测试类 
* @author zhengzhq E-Mail:zzq0324@qq.com
* @date 2016年9月30日 上午10:36:51 
*/
public class PropertyUtilTest {

    @Test
    public void testGetConfig() throws Exception {
        Properties prop = PropertyUtil.getConfig("c3p0-default");
        Assert.assertNotNull(prop);
        Assert.assertEquals(prop.get("c3p0.driverClass"), "org.hsqldb.jdbcDriver");
        Assert.assertFalse(prop.containsKey("c3p0.maxStatements"));

        prop = PropertyUtil.getConfig("c3p0-custom");
        Assert.assertNotNull(prop);
        Assert.assertEquals(prop.get("c3p0.driverClass"), "org.hsqldb.jdbcDriver");
        Assert.assertTrue(prop.containsKey("c3p0.maxStatements"));

        try {
            prop = PropertyUtil.getConfig("test");
            Assert.fail();
        } catch (Exception e) {
            Assert.assertTrue(e.getMessage().contains("can't find config file by configName"));
        }
    }

}
