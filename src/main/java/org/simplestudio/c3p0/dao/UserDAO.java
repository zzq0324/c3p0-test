package org.simplestudio.c3p0.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;

/**   
* @Title: UserDAO.java 
* @Description: 用户DAO 
* @author zhengzhq E-Mail:zzq0324@qq.com
* @date 2016年9月30日 下午1:40:00 
*/
public class UserDAO {

    public void query(Connection connection, long id) throws Exception {
        try {
            String sql = "select * from t_user where id=?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setLong(1, id);
            ps.executeQuery();

            ps.close();
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

}
